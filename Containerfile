FROM docker.io/python:3.9

ENV PYTHONUNBUFFERED 1
ENV BOOKWYRM_HOME /var/lib/bookwyrm

# Install required system packages
RUN apt-get -q -y update \
    && DEBIAN_FRONTEND=noninteractive apt-get -q -y upgrade \
    && apt-get -q -y install \
        gettext \
        libgettextpo-dev \
        tidy \
    && apt-get -q clean \
    && rm -rf /var/lib/apt/lists/*

RUN pip install virtualenv

# Create ckan user
RUN useradd \
    --system \
    --uid 900 \
    --create-home --home-dir $BOOKWYRM_HOME \
    --shell /bin/false \
    bookwyrm

WORKDIR $BOOKWYRM_HOME
USER bookwyrm

RUN virtualenv .venv

COPY --chown=bookwyrm source/requirements.txt ./

RUN .venv/bin/pip install -r requirements.txt --no-cache-dir

COPY --chown=bookwyrm source/manage.py ./
COPY --chown=bookwyrm source/bookwyrm/ ./bookwyrm/
COPY --chown=bookwyrm source/celerywyrm/ ./celerywyrm/
COPY --chown=bookwyrm source/locale/ ./locale/
COPY --chown=bookwyrm source/VERSION ./
COPY --chown=bookwyrm --chmod=755 scripts/container/entrypoint.sh ./

CMD $BOOKWYRM_HOME/entrypoint.sh
