worker_processes auto;

events {
}

http {
    include mime.types;

    charset     utf-8;
    etag on;

    # max upload size
    client_max_body_size 10m;

    # Cache
    limit_req_zone $binary_remote_addr zone=loginlimit:10m rate=1r/s;

    # include the cache status in the log message
    log_format cache_log '$upstream_cache_status - '
        '$remote_addr [$time_local] '
        '"$request" $status $body_bytes_sent '
        '"$http_referer" "$http_user_agent" '
        '$upstream_response_time $request_time';

    # Create a cache for responses from the web app
    proxy_cache_path
        /var/cache/nginx/bookwyrm_cache
        keys_zone=bookwyrm_cache:20m
        loader_threshold=400
        loader_files=400
        max_size=400m;

    # use the accept header as part of the cache key
    # since activitypub endpoints have both HTML and JSON
    # on the same URI.
    proxy_cache_key $scheme$proxy_host$uri$is_args$args$http_accept;

    upstream web {
        server localhost:8000;
    }

    server {
        listen $NGINX_LISTEN;
        server_name _;

        # access_log /var/log/nginx/access.log cache_log;

        sendfile on;
        tcp_nopush on;
        tcp_nodelay on;
        keepalive_timeout 65;
        types_hash_max_size 2048;

        gzip on;
        gzip_disable "msie6";

        proxy_read_timeout 1800s;
        chunked_transfer_encoding on;

        # store responses to anonymous users for up to 1 minute
        proxy_cache bookwyrm_cache;
        proxy_cache_valid any 1m;
        add_header X-Cache-Status $upstream_cache_status;

        # ignore the set cookie header when deciding to
        # store a response in the cache
        proxy_ignore_headers Cache-Control Set-Cookie Expires;

        # PUT requests always bypass the cache
        # logged in sessions also do not populate the cache
        # to avoid serving personal data to anonymous users
        proxy_cache_methods GET HEAD;
        proxy_no_cache      $cookie_sessionid;
        proxy_cache_bypass  $cookie_sessionid;

        # tell the web container the address of the outside client
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $host;
        proxy_redirect off;

        location ~ ^/(login[^-/]|password-reset|resend-link|2fa-check) {
            limit_req zone=loginlimit;
            proxy_pass http://web;
        }

        # do not log periodic polling requests from logged in users
        location /api/updates/ {
            access_log off;
            proxy_pass http://web;
        }

        location / {
            proxy_pass http://web;
        }

        # directly serve images and static files from the
        # bookwyrm filesystem using sendfile.
        # make the logs quieter by not reporting these requests
        location ~ \.(bmp|ico|jpg|jpeg|png|tif|tiff|webp|css|js|ttf|woff)$ {
            root /var/lib/bookwyrm;
            try_files $uri =404;
            add_header X-Cache-Status STATIC;
            access_log off;
        }

        # monitor the celery queues with flower, no caching enabled
        location /flower/ {
           proxy_pass http://localhost:8888;
           proxy_cache_bypass 1;
        }
    }
}
