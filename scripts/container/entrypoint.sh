#!/bin/bash

set -e

echo "### Running migrations..."
.venv/bin/python manage.py migrate

echo "### Compiling themes..."
.venv/bin/python manage.py compile_themes

echo "### Collecting static files..."
.venv/bin/python manage.py collectstatic --noinput

echo "### Starting bookwyrm..."
exec .venv/bin/gunicorn bookwyrm.wsgi:application --bind 0.0.0.0:8000
