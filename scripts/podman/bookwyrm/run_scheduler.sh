#!/bin/bash

__dir=$(dirname $(realpath $0))
ENV_FILE=$__dir/../../../environment
image=registry.gitlab.com/categulario/podman-bookwyrm/bookwyrm

podman run -it \
    --rm \
    --pod bookwyrm-pod \
    --env-file=$ENV_FILE \
    --volume bookwyrm_static:/var/lib/bookwyrm/static:U \
    --volume bookwyrm_media:/var/lib/bookwyrm/images:U \
    --name bookwyrm-scheduler \
    $image .venv/bin/celery -A celerywyrm beat -l INFO --scheduler django_celery_beat.schedulers:DatabaseScheduler
