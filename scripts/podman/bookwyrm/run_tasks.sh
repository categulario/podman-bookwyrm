#!/bin/bash

__dir=$(dirname $(realpath $0))
ENV_FILE=$__dir/../../../environment
image=registry.gitlab.com/categulario/podman-bookwyrm/bookwyrm

podman run -it \
    --rm \
    --pod bookwyrm-pod \
    --env-file=$ENV_FILE \
    --volume bookwyrm_static:/var/lib/bookwyrm/static:U \
    --volume bookwyrm_media:/var/lib/bookwyrm/images:U \
    --name bookwyrm-tasks \
    $image .venv/bin/celery -A celerywyrm worker -l info -Q high_priority,medium_priority,low_priority,imports,broadcast
