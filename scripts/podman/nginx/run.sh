#!/bin/bash

image=registry.gitlab.com/categulario/podman-bookwyrm/bookwyrm-nginx
container=bookwyrm-nginx

podman run \
    -it --rm \
    --pod bookwyrm-pod \
    --volume bookwyrm_static:/var/lib/bookwyrm/static \
    --volume bookwyrm_media:/var/lib/bookwyrm/images \
    --name $container \
    $image
