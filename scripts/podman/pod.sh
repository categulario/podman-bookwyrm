#!/bin/bash

podman pod create \
    --network slirp4netns:allow_host_loopback=true \
    --name bookwyrm-pod \
    --publish 8000:80 \
    --replace
