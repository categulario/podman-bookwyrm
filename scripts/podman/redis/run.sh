#!/bin/bash

image=docker.io/library/redis:7

podman run -it \
    --rm \
    --pod bookwyrm-pod \
    --name bookwyrm-redis \
    $image
