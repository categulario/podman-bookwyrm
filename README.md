# Podman deployment of bookwyrm

Mostly following [the official
guide](https://docs.joinbookwyrm.com/install-prod-dockerless.html) but adapted
to a podman deployment. I'm using this in production and containers are
available.

## Upgrading bookwyrm

* enter the `source` directory
* switch to the `production` branch
* pull changes
* switch to the desired branch (`v0.6.6` for example)
* build the image
* tag it according to the bookwyrm release. If the release didn't change but
  something in the packaging did, increase the build number (prefix, like `-1`
  in `0.6.6-1`)
* push it.
* create a git tag according to the image tag
